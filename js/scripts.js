window.onload = function() {
    [].forEach.call(document.getElementsByTagName("map"),function(map) {
        [].forEach.call(document.getElementsByTagName("img"),function(img) {
            if ("#" + map.name === img.useMap) {
                var ImageMap = function(curMap, curImg) {
                        var n,
                            areas = curMap.getElementsByTagName('area'),
                            len = areas.length,
                            coords = [],
                            previousWidth = curImg.naturalWidth;
                        for (n = 0; n < len; n++) {
                            coords[n] = areas[n].coords.split(',');
                        }
                        this.resize = function() {
                            var n, m, clen,
                                x = curImg.offsetWidth / previousWidth;
                            for (n = 0; n < len; n++) {
                                clen = coords[n].length;
                                for (m = 0; m < clen; m++) {
                                    coords[n][m] *= x;
                                }
                                areas[n].coords = coords[n].join(',');
                            }
                            previousWidth = document.body.clientWidth;
                            return true;
                        };
                        window.onresize = this.resize;
                    },

                    imageMap = new ImageMap(map, img);
                imageMap.resize();
            }
        })
    })

};